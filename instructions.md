## Before you begin
This task is based on CI framework.

Once you cloned this project, 

- Install a CI latest version
- Create database called `backend-test`. and import `backend-test.sql`. Once done you will have all the table with the required datasets which you will need to do the work.

## Instructions

Tasks
- You will be creating an endpoint to fetch order details. 
- Auth setup with two users type `admin` and `customer`. Show a default page on both the logins which contains text i.e. 'Welcome Customer' or 'Welcome Admin'.
- Create a form and implement Google's (https://developers.google.com/places/web-service/intro). You should be able to fetch lat and long of the result. Log the value in your browser console.

You can use any architecture you prefer. We are looking for a smart solution/ architecture / pattern in your coding. Code commenting will be preffered.


#### FOR API TASK
You need to specify a route `/orders/{id}`. basically when someone try to get order details they should get order details as a json payload. we need exact data format with `200` status (check the format at the end of this documentation). if someone try to fetch an order which is not in the database, he should get `400` status code as response. 


### Database relationships

Here we have mentioned all the relationships you need.

<img src="https://www.mysqltutorial.org/wp-content/uploads/2009/12/MySQL-Sample-Database-Schema.png">



#### json payload for existing order number (/api/orders/10100)


    {
       "order_id":10100,
       "order_date":"2003-01-06",
       "status":"Shipped",
       "order_details":[
          {
             "product":"1917 Grand Touring Sedan",
             "product_line":"Vintage Cars",
             "unit_price":136,
             "qty":30,
             "line_total":"4080.00"
          },
          {
             "product":"1911 Ford Town Car",
             "product_line":"Vintage Cars",
             "unit_price":55.09,
             "qty":50,
             "line_total":"2754.50"
          },
          {
             "product":"1932 Alfa Romeo 8C2300 Spider Sport",
             "product_line":"Vintage Cars",
             "unit_price":75.46,
             "qty":22,
             "line_total":"1660.12"
          },
          {
             "product":"1936 Mercedes Benz 500k Roadster",
             "product_line":"Vintage Cars",
             "unit_price":35.29,
             "qty":49,
             "line_total":"1729.21"
          }
       ],
       "bill_amount":"10223.83",
       "customer":{
          "first_name":"Dorothy",
          "last_name":"Young",
          "phone":"6035558647",
          "country_code":"USA"
       }
    }
 